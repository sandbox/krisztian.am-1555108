<h1>
     <a href="<?php print $reviews['url'] ?>" target="_blank"><?php print $reviews['name']; ?></a>
   </h1>

   <div class="yelp_reviews">
     <img src="<?php print $reviews['rating_img_url']; ?>"
          alt="<?php print $reviews['rating'] . " " . t('star rating'); ?>"
          class="yelp_reviews_rating"/>
     <div class="yelp_reviews_reviewcount"><?php print $reviews['review_count'] . " " . t('reviews'); ?></div>
     <?php
     if ($reviews['show_excerpts'] && is_array($reviews['reviews'])):
     foreach ($reviews['reviews'] as $excerpt):
     ?>
         <div class="yelp_reviews_usercolumn">
           <img src="<?php print $excerpt['user']['image_url']; ?>"
                alt="<?php print $excerpt['user']['name']; ?>"
                class="yelp_reviews">
           <div><?php print $excerpt['user']['name']; ?></div>
         </div>
         <div class="yelp_reviews_reviewcolumn">
           <img src="<?php print $excerpt['rating_image_url']; ?>"
                alt="<?php $excerpt['rating'] . " " . t('star rating'); ?>"
                class="yelp_reviews_singlerating">
           <div class="yelp_reviews_reviewdate"><?php print format_date($excerpt['time_created'], 'short'); ?></div>
           <div class="yelp_reviews_reviewexcerpt"><?php print $excerpt['excerpt']; ?></div>
           <a href="<?php print $reviews['url'] . "#hrid:" . $excerpt['id']; ?>" alt="Read more" title="Read the review"><?php print t('read more'); ?></a>
         </div>
         <div style="clear:both"></div>
     <?php
     endforeach;
     else:
     ?>
       <a target="_blank" href="<?php print $reviews['url']; ?>"><?php print t('read reviews'); ?></a>
     <?php
     endif;
     ?>
  </div>
